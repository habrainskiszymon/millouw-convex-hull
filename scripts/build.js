import shelljs from "shelljs";

const fatalOption = {'fatal': true};
const cwd = process.cwd();

console.log('Running ./scripts/build.js...');

console.log('Transpile typescript source files...');
shelljs.exec('tsc --inlineSourceMap --inlineSources', fatalOption, (code, stdout, stderr) => {
  if(code > 0){
    console.error("Transpilation failed. Errors should have appeared in stdout.");
    process.exit(1);
  }
});

console.log('Copy *.html files to /dist...');

shelljs.mkdir( cwd + '/dist');

shelljs.ls(cwd + '/src/*.html').forEach((file) => shelljs.cp(
    file,
    cwd + '/dist',
));

shelljs.ls(cwd + '/src/*.js').forEach((file) => shelljs.cp(
    file,
    cwd + '/dist',
));
