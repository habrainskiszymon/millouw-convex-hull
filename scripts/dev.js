import shelljs from "shelljs";

const fatalOption = {'fatal': true};
const cwd = process.cwd();

console.log("Running ./scripts/dev.js...");

console.log('Run prettier on *.html files...');
shelljs.exec(`prettier --write ${cwd + '/src/*.html'}`, fatalOption);

console.log('Run eslint on *.ts files...');
shelljs.exec(`eslint --fix ${cwd + '/src/*.ts'} ${cwd + '/src/*.js'} --max-warnings 0`, fatalOption);
