import Line from "./Line";
import Point from "./Point";
import Vector from "./Vector";

export default class CHAnimation {
    private readonly duration: number;

    private readonly convexHull: Line[];
    private visibleConvexHull: Line[];
    private animationHead: number;
    private lengthOfAllCompleteLines: number;
    private lengthSurplus: number;
    private currTick: number;
    private readonly totalLength: number;

    constructor(convexHull: Line[], duration: number) {
        if (convexHull.length < 3 || convexHull[0].start != convexHull[convexHull.length - 1].end)
            throw new Error('Invalid CH provided. A CH must be given as a closed path with a size of at least 3.');

        if (duration <= 0)
            throw new Error('Animation duration must be positive.');

        this.convexHull = convexHull;
        this.visibleConvexHull = [];
        this.duration = duration;
        this.animationHead = 0;
        this.lengthOfAllCompleteLines = 0;
        this.lengthSurplus = 0;
        this.currTick = -1;
        this.totalLength = convexHull
            .map(
                line => line.getLength()
            )
            .reduce(
                (acc,len) => acc + len, 0
            );
        // console.log("New animation", "totLen:",this.totalLength, "CH", convexHull);
    }

    tickNoToVisibleLength(tickNo: number){
        const that = this;
        function linear(tickNo: number): number{
            return that.totalLength * (tickNo / that.duration);
        }

        function accelerating(tickNo: number) {
            return (that.totalLength / 2) * Math.sin(((tickNo * Math.PI) / that.duration) - Math.PI / 2) + that.totalLength / 2;
        }

        return accelerating(tickNo);
    }

    finished(): boolean {
        return this.lengthOfAllCompleteLines + this.lengthSurplus == this.totalLength;
    }

    tick(): Line[] {
        // console.log(this.convexHull, this.animationHead, this.lengthOfAllCompleteLines, this.lengthSurplus, this.totalLength);

        if (this.finished())
            throw new Error('Animation has already finished.');

        this.currTick++;

        const newVisibleLength = this.tickNoToVisibleLength(this.currTick);
        // console.log(newVisibleLength);
        let additionalVisibleLength = newVisibleLength - (this.lengthOfAllCompleteLines + this.lengthSurplus);

        if (additionalVisibleLength == 0)
            return this.visibleConvexHull;

        if ((this.convexHull[this.animationHead].getLength() - this.lengthSurplus) > additionalVisibleLength) {
            // cannot fill the head
            const headline = this.convexHull[this.animationHead];
            if (this.lengthSurplus > 0)
                this.visibleConvexHull.pop();
            this.visibleConvexHull.push(
                new Line(
                    headline.start,
                    Vector.add(
                        headline.start,
                        Vector.mult(headline.asVector(), (1 / headline.asVector().mag()) * (this.lengthSurplus + additionalVisibleLength))
                    )
                )
            )
            this.lengthSurplus += additionalVisibleLength;
        } else {
            // we can "complete" the head an move on
            if (this.lengthSurplus > 0)
                this.visibleConvexHull.pop();
            this.visibleConvexHull.push(this.convexHull[this.animationHead]);
            const animationHeadLength = this.visibleConvexHull[this.visibleConvexHull.length - 1].getLength();
            this.lengthOfAllCompleteLines += animationHeadLength;
            additionalVisibleLength -= animationHeadLength;
            this.animationHead++;
            this.lengthSurplus = 0;

            if (this.animationHead >= this.convexHull.length && !this.finished())
                throw new Error('Out of bounds with animationHead in the convexHull. Maybe something went wrong with tickNoToVisibleLength(tickNo)?');


            for(; this.animationHead < this.convexHull.length && additionalVisibleLength > 0; this.animationHead++)
            {
                const headline = this.convexHull[this.animationHead];
                const headlineLength = headline.getLength();
                if (headlineLength <= additionalVisibleLength) {
                    this.visibleConvexHull.push(headline);
                    additionalVisibleLength -= headlineLength;
                    this.lengthOfAllCompleteLines += headlineLength;
                } else {
                    this.visibleConvexHull.push(
                        new Line(
                            headline.start,
                            Vector.add(
                                headline.start,
                                Vector.mult(headline.asVector(), (1 / headline.asVector().mag()) * additionalVisibleLength)
                            )
                        )
                    )
                    this.lengthSurplus = additionalVisibleLength;
                    break;
                }
            }
        }
        // console.log(this.visibleConvexHull);
        return this.visibleConvexHull;
    }
}
