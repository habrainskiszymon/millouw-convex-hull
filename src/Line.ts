import Vector from "./Vector";
import RenderableInterface from "./RenderableInterface";
import App from "./App";

export default class Line implements RenderableInterface{
    private readonly _start: Vector;
    private readonly _end: Vector;

    constructor(start: Vector, end: Vector) {
        this._start = start;
        this._end = end;
    }

    asVector(): Vector {
        return Vector.sub(this._end, this._start);
    }

    getLength() { return Vector.sub(this.end, this.start).mag() }

    get start() { return this._start; }
    get end() { return this._end; }

    public render(ctx: CanvasRenderingContext2D) {
        ctx.lineWidth = App.theme.lineThickness;
        ctx.strokeStyle= "#FF0000";
        ctx.beginPath();
        ctx.moveTo(this.start.x, this.start.y);
        ctx.lineTo(this.end.x, this.end.y);
        ctx.stroke();
    }
}
