import express from "express";
import shelljs from "shelljs";
const {which} = shelljs;

const appName = 'millouw-convex-hull';
const port = process.env.PORT || 8080;
const app = express();
const servingDir = process.cwd() + '/dist';

console.log('Serving directory: ', servingDir);

app.use('/', (req, res, next) => {
  // support es6 modules without js extension
  const urlMatch = req.url.match(/.([^.]+)$/);
  if (urlMatch && !['html', 'css', 'js', 'ico'].includes(urlMatch[1])) {
    console.log(`Serving ${req.url} as ${req.url + '.js...'}`);
    req.url += '.js';
  }
  next();
});
app.use('/', express.static(servingDir));

if (which('fuser')) {
  // Kills process that uses port.
  shelljs.exec(`fuser -k ${port}/tcp`, {'silent': true});
}

app.listen(port, () => {
  console.log(`Serving ${appName} @ ${port}...`);
});
