import App from './App';

window.addEventListener( 'load', () => {
  const canvas: HTMLElement | null = document.getElementById('canvas');

  if (canvas instanceof HTMLCanvasElement) {
    const ctx: CanvasRenderingContext2D | null = canvas.getContext('2d');
    if (ctx == null) {
      return doCrash('Unable to obtain rendering context.');
    }

    try {
      const width = window.innerWidth;
      const height = window.innerHeight;
      const dpr = devicePixelRatio?? 1;
      canvas.style.width = width + "px";
      canvas.style.height = height + "px";
      canvas.width = Math.floor(width * dpr);
      canvas.height = Math.floor(height * dpr);


      App.initialize({
        canvas,
        rate: 60
      });
      App.start();
    } catch (error: any) {
      console.error(error);
      doCrash(error.message);
    }
  } else {
    return doCrash('Unable to find canvas element.');
  }
});

/**
 * Displays a readable crash message in the DOM.
 * @param {string} msg Message to be displayed.
 */
function doCrash(msg: string) {
  console.error(msg);

  const errorDiv: HTMLElement | null = document.getElementById('error');
  const crashHeadline: HTMLHeadingElement = document.createElement('h2');
  const crashMessage: HTMLParagraphElement = document.createElement('p');

  crashHeadline.innerText = 'The app crashed. Sorry...';
  crashHeadline.style.color = '#FF0000';
  crashMessage.innerText = msg;

  if (errorDiv instanceof HTMLDivElement) {
    errorDiv.appendChild(crashHeadline);
    errorDiv.appendChild(crashMessage);
    errorDiv.hidden = false;
  }
}
