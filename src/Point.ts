import Vector from "./Vector";
import RenderableInterface from "./RenderableInterface";
import App from "./App";

export default class Point extends Vector implements RenderableInterface {
    public static readonly SELECTION_EPS: number = 10;

    private readonly _radius: number;
    private _selected: boolean; // whenever mouse is over this point
    private _dragged: boolean; // whenever this point is currently dragged to some other position

    constructor(x: number, y: number, radius: number) {
        super(x, y);

        if (radius <= 0)
            throw new Error("Radius must be positive.");

        this._radius = radius;
        this._selected = false;
        this._dragged = false;
    }

    public get radius() { return this._radius; }

    public isSelected(): boolean {  return this._selected; }
    public select(): void { this._selected = true; }
    public unselect(): void { this._selected = false; }

    public isDragged(): boolean { return this._dragged; }
    public drag(): void { this._dragged = true; }
    public drop(): void { this._dragged = false; }

    public isInside(u: Vector): boolean {
        const dist: number = Vector.sub(u, this).mag();
        return dist <= this.radius + Point.SELECTION_EPS;
    }

    public render(ctx: CanvasRenderingContext2D) {
        const outerRadius: number = this.radius * (this.isDragged()? App.theme.pointDraggedScaling : this.isSelected()? App.theme.pointHighlightScaling: 1);
        const innerRadius = App.theme.pointInnerPortion * outerRadius;

        ctx.fillStyle = App.theme.pointOuterColor;
        ctx.beginPath();
        ctx.arc(this.x, this.y, outerRadius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
        ctx.fillStyle = App.theme.pointInnerColor;
        ctx.beginPath();
        ctx.arc(this.x, this.y, innerRadius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fill();
    }
}
