// eslint-disable-next-line require-jsdoc
export default class Vector {
    private _x: number;
    private _y: number;

    // eslint-disable-next-line require-jsdoc
    constructor(x: number, y: number) {
      this._x = Math.floor(x);
      this._y = Math.floor(y);
    }

    // eslint-disable-next-line require-jsdoc
    get x(): number {
      return this._x;
    }

    set x(newX: number) {
        this._x = newX;
    }

    // eslint-disable-next-line require-jsdoc
    get y() : number {
      return this._y;
    }

    set y(newY: number) {
        this._y = newY;
    }
    // eslint-disable-next-line require-jsdoc
    public static add(u: Vector, ...others:Vector[]): Vector {
        let [xSum, ySum] = [u, ...others].reduce<[number, number]>(
            (xySum: [number, number], v:Vector) =>
                [xySum[0] + v.x, xySum[1] + v.y],
            [0, 0]
        );

      return new Vector(xSum, ySum);
    }

    public static sub(u: Vector, ...others:Vector[]): Vector {
        return Vector.add(u, ...others.map(other => Vector.mult(other, -1)));
    }

    private static assertIsInterval(interval : [number, number]){
        if (!(interval[0] <= interval[1])) {
            throw new Error("Assertion failed: No proper interval provided." +
                " Left bound must be smaller or equal to the right bound.")
        }
    }

    public static mult(u: Vector, scalar: number){
        return new Vector(u.x * scalar, u.y * scalar);
    }

    public static addUnderBounds(
        xBound : [number, number],
        yBound: [number, number],
        u: Vector,
        ...others:Vector[]
    ): Vector {
        Vector.assertIsInterval(xBound);
        Vector.assertIsInterval(yBound);

        let unboundedVector: Vector = Vector.add(u, ...others);
        const boundedX = Math.max(Math.min(unboundedVector.x, xBound[1]), xBound[0]);
        const boundedY = Math.max(Math.min(unboundedVector.y, yBound[1]), yBound[0]);

        return new Vector(boundedX, boundedY);
    }

    public mag(): number {
      return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
    }

    public isOnTheLeftOf(u: Vector): boolean{
        const determinant = u.x * this.y - u.y * this.x;

        return determinant > 0; // two collinear vectors are neither left nor right
    }

    public static getRandomVector(
        xMin: number, xMax: number,
        yMin: number, yMax: number,
    ): Vector {
      if (xMin > xMax || yMin > yMax) {
        throw Error('Invalid range(s) provided.');
      }

      const newX: number = xMin + Math.random() * (xMax - xMin);
      const newY: number = yMin + Math.random() * (yMax - yMin);

      return new Vector(newX, newY);
    }

    // eslint-disable-next-line require-jsdoc
    public toString() : string {
      return `Point(${this.x},${this.y})`;
    }
}
