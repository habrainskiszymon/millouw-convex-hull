export default interface RenderableInterface {
    render(context: CanvasRenderingContext2D): void;
}
