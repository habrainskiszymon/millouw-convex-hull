import Vector from './Vector';
import Point from "./Point";
import Line from "./Line";
import RenderableInterface from "./RenderableInterface";
import CHAnimation from "./CHAnimation";
import Timeout = NodeJS.Timeout;

export type Theme = {
  primaryColor: string;
  backgroundColor: string;

  pointSize: number,
  pointInnerColor: string,
  pointOuterColor: string
  pointInnerPortion: number
  pointHighlightScaling: number
  pointDraggedScaling: number,
  lineThickness: number
}

type AppConfig = {
  canvas: HTMLCanvasElement,
  theme?: Theme,
  rate?: number
}

type State = {
    points: Point[],
    lines: Line[],
    dragged: Point | null
}

export default class App {
  private static readonly MAX_RATE = 60;
  private static readonly MIN_RATE = 30;
  private static readonly MAX_TICKS = 3000;

  private static _canvas: HTMLCanvasElement;
  private static _ctx: CanvasRenderingContext2D;
  // App initialization management
  private static _initialized: boolean;
  private static _state_initialized: boolean;

  // Timing
  private static _t0: number;
  private static _tickDuration: number;

  // Render Management
  private static _running: boolean;
  private static _frameReq: number;
  private static _rate: number; // in seconds
  private static _dpr: number;
  // cache width/ height
  private static _width: number;
  private static _height: number;

  // Application state
  private static _state: State | null;


  // Theme
  private static _theme: Theme;

  // CH Animation
  private static _chAnimation: CHAnimation | null;
  private static _animating: number // -1: nothing; 0: in timeout; 1: active
  private static _timeoutID: NodeJS.Timeout;
  private static _isMobile: boolean;

  public static initialize(config: AppConfig, theme?: Theme) : void {
    if(App._initialized)
      throw new Error('Cannot initialize app twice.');

    App._canvas = config.canvas;
    App._width = App._canvas.width;
    App._height = App._canvas.height;

    const ctx: CanvasRenderingContext2D | null = App._canvas.getContext("2d");
    if (ctx == null)
      throw new Error("Cannot get canvas rendering context ('2d').");
    App._ctx = <CanvasRenderingContext2D>ctx;

    if (!('rate' in config))
      config.rate = App.MIN_RATE;

    config.rate = Math.max(App.MIN_RATE, Math.min(App.MAX_RATE, <number>config.rate));

    // App._t0 is set when the app starts
    App._rate = config.rate;
    App._tickDuration = 1000 / (App._rate); // App._rate is given in seconds
    App._dpr = window.devicePixelRatio? window.devicePixelRatio: 1;

    config.theme = config.theme ?? {
      primaryColor: '#BBBBBB',
      backgroundColor: '#DDDDDD',
      pointSize: 8 * App._dpr,
      pointInnerPortion: 0.66,
      pointInnerColor: '#FFFFFF',
      pointOuterColor: '#000000',
      pointHighlightScaling:  1.1,
      pointDraggedScaling: 0.9,
      lineThickness: 2 * App._dpr
    }

    const validInnerPortion: boolean = config.theme.pointInnerPortion > 0 && config.theme.pointInnerPortion <= 1
    const validHighlightScaling: boolean = config.theme.pointHighlightScaling >= 1;

    if (!validInnerPortion)
      throw new Error('Theme.pointInnerPortion must be in the interval (0,1].');


    if (!validHighlightScaling) {
      throw new Error('Theme.pointHighlightScaling must be greater or equal to 1');
    }

    App._theme = config.theme;

    App._isMobile = false;

    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
      const ua = navigator.userAgent.toLowerCase();
      if (ua.indexOf('safari') != -1) {
        if (ua.indexOf('chrome') <= -1) {
          throw new Error("Safari on mobile devices is not supported.");
        }
      }
      App._isMobile = true;
    }

    App._chAnimation = null;
    App._animating = -1;
    App._state_initialized = false;
    App._initialized = true;
  }

  static get width(): number {
    return App._width;
  }

  static get height(): number {
    return App._height;
  }

  static get dpr(): number {
    return App._dpr;
  }

  static get theme(): Theme { return App._theme; }

  private static registerListeners(){
  if (App._isMobile){
    App._canvas.addEventListener("touchstart", App.convertTouchEventIntoMouseEvent);
    App._canvas.addEventListener("touchmove", App.convertTouchEventIntoMouseEvent);
    App._canvas.addEventListener("touchend", App.convertTouchEventIntoMouseEvent);
    App._canvas.addEventListener("touchcancel", App.convertTouchEventIntoMouseEvent);
  } else {
    App._canvas.addEventListener("mousedown", App.mouseDownListener);
    App._canvas.addEventListener("mousemove", App.mouseMoveListener);
    App._canvas.addEventListener("mouseup", App.mouseUpListener);
    App._canvas.addEventListener("mouseout", App.mouseLeaveListener);
  }
  }

  private static unregisterListeners(){
    if (App._isMobile){
      App._canvas.removeEventListener("touchstart", App.convertTouchEventIntoMouseEvent);
      App._canvas.removeEventListener("touchmove", App.convertTouchEventIntoMouseEvent);
      App._canvas.removeEventListener("touchend", App.convertTouchEventIntoMouseEvent);
      App._canvas.removeEventListener("touchcancel", App.convertTouchEventIntoMouseEvent);
    } else {
      App._canvas.removeEventListener("mousedown", App.mouseDownListener);
      App._canvas.removeEventListener("mousemove", App.mouseMoveListener);
      App._canvas.removeEventListener("mouseup", App.mouseUpListener);
      App._canvas.removeEventListener("mouseout", App.mouseLeaveListener);
    }
  }


  private static computeMouseInTransformedSystem(event: MouseEvent): Vector {
    // console.log(event.clientX, event.clientY);
    const mouseX = App._dpr * (event.clientX);
    const mouseY = App._dpr * (event.clientY);

    return new Vector(mouseX, mouseY);
  }

  private static mouseUpListener() {
    console.log("up");
    App._state = <State> App._state;
      // drop
      if (App._state.dragged != null)
        App._state.dragged.drop();
      App._state.dragged = null;
  }

  private static mouseLeaveListener(event: MouseEvent) {
    console.log("leave");
    App.mouseUpListener();
  }

  private static mouseDownListener(event: MouseEvent){
    console.log("down");
    App._state = (<State> App._state);

    const mouse: Vector = App.computeMouseInTransformedSystem(event);

    const selectedPoints = App.getSelectedPoints(mouse);

    if (selectedPoints[1].length != 0){
      // Interpret click as a point drag operation
      const pointToBeDragged = selectedPoints[1][0];
      if (App._state.dragged != null) {
        // Previous point must have been dropped before
        throw new Error("There must not be a point to be dragged" +
            " when a mouse click has been registered.");
      }

      App._state.dragged = pointToBeDragged; // drag
      App._state.dragged.drag();
    } else {
      // Interpret click as a point creation operation
      App._state.points.push(
          new Point(
              mouse.x,
              mouse.y,
              App.theme.pointSize
          )
      );
    }

    App.computeConvexHull();
  }

  private static mouseMoveListener(event: MouseEvent){
    console.log("move");

    App._state = (<State> App._state);

    const mouse: Vector = App.computeMouseInTransformedSystem(event);

    const selectedPoints: [Point[], Point[]] = App.getSelectedPoints(mouse);
    const pointsChangedInSelection = [...selectedPoints[0], ...selectedPoints[1]];

    for (let point of pointsChangedInSelection) {
        point.isInside(mouse)? point.select(): point.unselect();
    }

    if (App._state.dragged != null) {
      App._state.dragged.x = mouse.x;
      App._state.dragged.y = mouse.y;
      App.computeConvexHull();
    }
  }

  private static convertTouchEventIntoMouseEvent(touchEvent: TouchEvent){
    touchEvent.preventDefault();
    function getEvent(mEventName: string){
      return new MouseEvent(
          mEventName,
          {
            clientX: touchEvent.touches[0].clientX,
            clientY: touchEvent.touches[0].clientY
          }
      );
    }
    if (touchEvent.touches.length == 0)
      return App.mouseUpListener();

    let mouseEvent: string;
    switch (touchEvent.type) {
      case "touchstart":
        return App.mouseDownListener(getEvent("mousedown"));
      case "touchmove":
        return App.mouseMoveListener(getEvent("mousemove"));
      case "touchcancel":
        return App.mouseLeaveListener(getEvent("mouseout"));
      default:
        throw new Error("Misuse of convertTouchEventIntoMouseEvent(touchEvent: TouchEvent).")
    }
  }

  // return [selection_lost, selection_gained]
  private static getSelectedPoints(mouse: Vector): [Point[], Point[]] {
    const selectionChange: [Point[], Point[]] = [[], []]

    for (let point of (<State> App._state).points) {
      const selected = point.isInside(mouse);
      if (selected) {
        selectionChange[1].push(point);
      } else if (point.isSelected()) {
        selectionChange[0].push(point);
      }
    }

    return selectionChange;
  }

  private static loop(): void {
      const t1: number = performance.now();
      const tDiff: number = t1 - App._t0;
      const ticks: number = Math.floor(tDiff / App._tickDuration);

      if (ticks > App.MAX_TICKS)
        throw Error('Number of ticks to be processed exceeded App.MAX_TICKS');

      App.render(ticks);

      App._t0 += ticks * App._tickDuration;
      App._frameReq = requestAnimationFrame(App.loop);
  }

  private static clear() {
    const context = App._ctx;
    context.save();
    context.fillStyle = App.theme.backgroundColor;
    App._ctx.rect(0,0, App.width, App.height);
    context.fill();
    context.restore();
  }

  private static render(ticks: number): void {
    App.clear();
    App._state = <State> App._state;

    if (this._animating == 1) {
      if (App._chAnimation != null){
        if (App._chAnimation.finished()){
          this._animating = -1;
          App._chAnimation = null;
        } else {
          App.renderComponents(App._chAnimation.tick());
        }
      }
      else
        throw new Error ("Animation flag set without having an animation object.");
    } else {
        App.renderComponents(App._state.lines);
    }

    App.renderComponents(App._state.points);
  }

  private static renderComponents(components: RenderableInterface[]){
    components.forEach(component => this.renderComponent(component));
  }

  private static renderComponent(component: RenderableInterface){
    App._ctx.save();
    component.render(App._ctx);
    App._ctx.restore();
  }

  public static start(): void {
    console.log("Starting app...");

    if (App._running)
      throw new Error('Cannot start running app.');

    if (!App._state_initialized)
      App.initializeState();

    App._t0 = performance.now();
    window.requestAnimationFrame(App.loop);
    App._running = true;
  }

  public static polarCoordinates(u: Vector): [number, number] {
    const r : number = u.mag();
    const alpha = Math.atan2(u.y, u.x);

    return [r, alpha];
  }

  public static computeConvexHull(): void{
    // Graham Scan
    App._state = <State> App._state;
    const pts = App._state.points;

    if(pts.length < 3) return;

    let lowestPtIdx = 0;
    for (let i = 1; i < pts.length; i++) {
        if (pts[i].y > pts[lowestPtIdx].y) {
          lowestPtIdx = i;
        } else if (pts[i].y == pts[lowestPtIdx].y && pts[i].x < pts[lowestPtIdx].x) {
          lowestPtIdx = i;
        }
    }

    const lowestPt = pts[lowestPtIdx];

    const polarSortedPts: number[] =
        App._state.points
            .map(
                (point: Point, idx: number) => {
                  return {
                    polar: App.polarCoordinates(
                        Vector.sub(point, lowestPt)
                    ),
                    idx
                  }
                }
            )
            .sort(
                (
                    {polar: polarP},
                    {polar: polarQ}
                ) => polarP[1] == polarQ[1] ? polarP[0] - polarQ[0] : polarQ[1] - polarP[1]
            )
            .map(
                ({polar, idx}) => idx
            );

    // console.log(pts[lowestPtIdx], polarSortedPts.map(idx => pts[idx]));

    const st = [polarSortedPts[0], polarSortedPts[1]];
    let i = 2;
    while(i < polarSortedPts.length){
      const p: Point = pts[st[st.length - 2]];
      const q: Point = pts[st[st.length - 1]];
      const r: Point = pts[polarSortedPts[i]];

      const pr = Vector.sub(r, p);
      const pq = Vector.sub(q, p);

      // console.log(p, q, r, pq.isOnTheLeftOf(pr));

      if (pq.isOnTheLeftOf(pr) || st.length == 2) {
        st.push(polarSortedPts[i]);
        i++;
      } else {
        st.pop();
      }
    }

    // emit the convex hull
    st.push(st[0]); // we want a closed structure
    App._state.lines = [];
    for (let i = 0; i < st.length - 1; i++)
        App._state.lines.push(new Line(pts[st[i]], pts[st[i + 1]]));

    if(App._animating == 0)
      // Stop given animation because the convex hull might have changed now
      clearTimeout(App._timeoutID);


    // Creating a new animation
    this._chAnimation = new CHAnimation(App._state.lines, 1 * 1000/this._tickDuration);
    this._animating = 0;
    this._timeoutID = setTimeout(() => {this._animating = 1}, 1000);
 }
  public static terminate(): void {
    console.log("Terminating app...")
    if (!App._running)
      throw new Error('Cannot terminate app that has not been started.');

    window.cancelAnimationFrame(App._frameReq);
    App.terminateState();

    App._running = false;
  }

  private static initializeState(): void {
    App._state = {points: [], lines: [], dragged: null};

    App.registerListeners();
    App._state_initialized = true;
  }

  private static terminateState(): void {
    App._state = null;
    App.unregisterListeners();
    App._state_initialized = false;
  }
}
